class HumanPlayer
  def initialize(name)
    @name = name
  end

  attr_accessor :mark
  attr_reader :name

  def get_move
    print "where do you want to move (e.g. 0, 0)?"
    input = gets.chomp
    parse(input)
  end


  def display(board)
    board.grid.each do |row|
      print row
      print "\n"
    end
  end

  private

  def parse(input)
    input.split(", ").map(&:to_i)
  end

end
