class Board
  def initialize(grid = [[nil, nil, nil], [nil, nil, nil], [nil, nil, nil]])
    @grid = grid
  end

  attr_accessor :grid

  def place_mark(position, mark)

    unless empty?(position)
      raise Exception("Position #{position} is not empty.")
    end
    grid[position.first][position.last] = mark
  end

  def empty?(position)
    grid[position.first][position.last] == nil
  end

  def winner
    # row & column
    0.upto(2) do |index|
      # p index

      cell1 = grid[index][0]
      cell2 = grid[index][1]
      cell3 = grid[index][2]
      if cell1 != nil && cell1 == cell2 && cell1 == cell3
        return cell1
      end
      cell1 = grid[0][index]
      cell2 = grid[1][index]
      cell3 = grid[2][index]
      if cell1 != nil && cell1 == cell2 && cell1 == cell3
        return cell1
      end
    end
    # diagonal
    cell1 = grid[0][0]
    cell2 = grid[1][1]
    cell3 = grid[2][2]
    if cell1 != nil && cell1 == cell2 && cell1 == cell3
      return cell1
    end
    cell1 = grid[0][2]
    cell2 = grid[1][1]
    cell3 = grid[2][0]
    if cell1 != nil && cell1 == cell2 && cell1 == cell3
      return cell1
    end
  end

  def over?
    winner != nil || full?(grid)
  end

  private

  def full?(grid)
    grid.each do |row|
      row.each do |cell|
        return false if cell == nil
      end
    end
    true
  end

end
