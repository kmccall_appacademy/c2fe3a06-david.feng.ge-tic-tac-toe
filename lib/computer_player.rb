class ComputerPlayer

  def initialize(name)
    @name = name
  end

  attr_accessor :mark
  attr_reader :name, :board

  def get_move
    empty_positions = empty_positions(@board)
    empty_positions.each do |empty_position|
      temp_board = board
      temp_board.place_mark(empty_position, mark)
      if temp_board.winner == mark
        return empty_position
      end
    end
    empty_positions.sample #pick one element randomly
  end


  def display(board)
    @board = board
  end

  private

  # returns an array of empty positions (array of two coordinates(integers))
  def empty_positions(board)
    empty_positions = []
    (0..2).each do |column|
      (0..2).each do |row|
        if board.empty?([row, column])
          empty_positions.push([row, column])
        end
      end
    end
    empty_positions
  end

end
