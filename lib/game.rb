require_relative 'board'
require_relative 'human_player'
require_relative 'computer_player'

class Game
  def initialize(player_one, player_two)
    @board = Board.new
    @current_player = player_one
    @waiting_player = player_two
  end

  attr_accessor :board, :current_player, :waiting_player

  def switch_players!
    # it seems these @ are essential, without them this line does not work
    # although we already have those attr_accessors
    @current_player, @waiting_player = @waiting_player, @current_player
  end

  def play_turn
    position = @current_player.get_move
    board.place_mark(position, @current_player.mark)
    switch_players!
  end

  def play
    until board.over?
      play_turn
    end
    if board.winner
      puts "The winner's mark is #{board.winner}"
    else
      puts "The game is tied"
    end
  end

end
